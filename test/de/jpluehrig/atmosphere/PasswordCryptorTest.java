package de.jpluehrig.atmosphere;

import static org.junit.Assert.*;

import org.junit.Test;

import de.jpluehrig.atmosphere.model.security.PasswordCrypt;

public class PasswordCryptorTest {

	@Test
	public void generateHashedPassword(){
		PasswordCrypt pc = new PasswordCrypt("Hello World");
		 
		assertEquals(true, PasswordCrypt.checkPassword("Hello World", pc));
		assertEquals(false, PasswordCrypt.checkPassword("Hello World2", pc));
	}
}
