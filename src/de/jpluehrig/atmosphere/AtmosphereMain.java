package de.jpluehrig.atmosphere;


import javax.servlet.annotation.WebServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;

import de.jpluehrig.atmosphere.gui.AttendanceView;
import de.jpluehrig.atmosphere.gui.MainUserView;
import de.jpluehrig.atmosphere.gui.login.LoginView;
import de.jpluehrig.atmosphere.gui.model.DataEditView;
import de.jpluehrig.atmosphere.gui.model.FireFighterView;
import de.jpluehrig.atmosphere.gui.model.ManageDutyView;
import de.jpluehrig.atmosphere.gui.model.UserSettingView;

import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("atmosphereproject")
public class AtmosphereMain extends UI {
	private boolean loggedIn = false;
	Navigator navigator;
    public static final String LOGINVIEW = "";
    public static final String MAINVIEW = "main";
    public static final String SETTINGSVIEW = "main/settings";
    public static final String FIREFIGHTERSETTINGSVIEW = "main/settings/firefighter";
    public static final String ATTENDANTVIEW = "main/attendance";
    public static final String DATAEDITVIEW = "main/dataedit";
    public static final String MANAGEDUTYVIEW = "main/manageduty";

	//public static final Logger logger = Logger.getLogger(AtmosphereMain.class.getName());
	public static final Logger logger = LogManager.getLogger(AtmosphereMain.class.getName());

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = AtmosphereMain.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		logger.debug("Call init page");
		getPage().setTitle("Atmosphere");
		navigator = new Navigator(this, this);

		final LoginView view = new LoginView();
		
		navigator.addView(LOGINVIEW, view);
        navigator.addView(MAINVIEW, new MainUserView());
        navigator.addView(SETTINGSVIEW, new UserSettingView());
        navigator.addView(FIREFIGHTERSETTINGSVIEW, new FireFighterView());
        navigator.addView(ATTENDANTVIEW, new AttendanceView());
        navigator.addView(DATAEDITVIEW, new DataEditView());
        navigator.addView(MANAGEDUTYVIEW, new ManageDutyView());
        //navigator.addView(SETTINGSVIEW, new MainUserView());
		//logger.trace("Open website." + request.getRemoteHost() + " " + request.getRemoteAddr());
		
		if(getSession().getAttribute("user") == null){
			logger.debug("No user is logged in.");
			navigator.navigateTo(LOGINVIEW);
		} else {
			logger.debug("User already logged in: "+getSession().getAttribute("user"));
		}
		//setContent(view);
		//navigator.navigateTo(LOGINVIEW);	
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
}