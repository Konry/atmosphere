package de.jpluehrig.atmosphere.gui;

import javax.servlet.http.Cookie;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import de.jpluehrig.atmosphere.AtmosphereMain;
import de.jpluehrig.atmosphere.gui.elements.ConfirmDialogCallback;
import de.jpluehrig.atmosphere.gui.elements.ConfirmationDialog;

public class MainUserView extends MainUserScreen implements ConfirmDialogCallback, View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8496970425287496284L;

	public MainUserView() {
		initialize();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	public void initialize() {
		AtmosphereMain.logger.debug("initialize MainDesignView");

		dataEditButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -6274127193409103764L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.trace("Data edit button clicked.");
				getUI().getNavigator().navigateTo(AtmosphereMain.DATAEDITVIEW);
			}
		});
		
		manageDutyButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -6274127193409103764L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.trace("Manage Duty button clicked.");
				// TODO Auto-generated method stub
				System.out.println("Draw cookie list:");
				Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
				for (Cookie c : cookies) {
					System.out.println(c.getName());
				}
				getUI().getNavigator().navigateTo(AtmosphereMain.MANAGEDUTYVIEW);
			}
		});

		attendanceButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -4840080148229498856L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.trace("Attendance button clicked.");
				// TODO Auto-generated method stub
				Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
				for (Cookie c : cookies) {
					System.out.println(c.getName());
				}
				/* Create new Event */
				
				/* Go to event */
				getUI().getNavigator().navigateTo(AtmosphereMain.ATTENDANTVIEW);
			}
		});

		settingsButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -6879841754227155076L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.trace("Settings button clicked.");
				getUI().getNavigator().navigateTo(AtmosphereMain.SETTINGSVIEW);
			}
		});

		logoutButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397489180445289102L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.trace("Logout button clicked.");
				new ConfirmationDialog("Ausloggen?", "Wollen Sie sich ausloggen?", MainUserView.this, "logOutAction");
			}
		});
	}

	//
	// private boolean verifyLogOutAction() {
	// /* replace later with touch option */
	//
	//// MessageBox.createQuestion().withCaption("Ausloggen?").withMessage().withOkButton(()
	// -> confirmLogOutAction())
	//// .withCancelButton(() -> cancelLogOutAction()).open();
	// }

	@Override
	public void CancelAction(String confirmActionName) {
		if (confirmActionName.equals("logOutAction")) {
			AtmosphereMain.logger.trace("Log out cancelled");
		}
	}

	@Override
	public void ConfirmAction(String confirmActionName) {
		AtmosphereMain.logger.trace("Log out confirmed.");
		if (confirmActionName.equals("logOutAction")) {
			getUI().getNavigator().navigateTo(AtmosphereMain.LOGINVIEW);
		}
	}

}
