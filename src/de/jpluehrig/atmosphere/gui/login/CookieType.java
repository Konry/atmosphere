package de.jpluehrig.atmosphere.gui.login;

public enum CookieType {
	LOGGEDIN("loggedin"), REMEMBERME("rememberme");

	private final String text;

	/**
	 * @param text
	 */
	private CookieType(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return text;
	}

	/**
	 * 
	 * @param text The value to parse
	 * @return The CookieType if existing, otherwise throws a null object
	 */
	public static CookieType parseCookieType(final String text) {
		switch (text) {
		case "loggedin":
			return LOGGEDIN;
		case "rememberme":
			return REMEMBERME;
		default:
			return null;
			//throw new IllegalArgumentException("Text of cookieType cannot be parsed into a cookieType.");
		}
	}
}
