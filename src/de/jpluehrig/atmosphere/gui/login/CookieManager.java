package de.jpluehrig.atmosphere.gui.login;

import javax.servlet.http.Cookie;

import com.thoughtworks.selenium.webdriven.commands.GetCookieByName;
import com.vaadin.server.VaadinService;

public class CookieManager {

	private static final String ATMOSPHERE_COOKIE_NAME = "atmosphere-last-login";

	private static final int secondsPerMonth = 60 * 60 * 24 * 31;

	public CookieManager() {
		// TODO Auto-generated constructor stub
	}

	public static void createCookie(String cookieName, String username, CookieType cookieType) {
		AtmosphereCookie myCookie = new AtmosphereCookie(cookieName, username, cookieType);
		myCookie.setMaxAge(secondsPerMonth);
		myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());
		VaadinService.getCurrentResponse().addCookie(myCookie);
	}

	public static void renewCookie(String cookieName, String username, CookieType cookieType) {
		AtmosphereCookie myCookie = getCookieByName(cookieName);
		myCookie.setMaxAge(secondsPerMonth);
		myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());
		myCookie.renewID();
		VaadinService.getCurrentResponse().addCookie(myCookie);
	}

	public static void expireCookie(String cookieName, String username, CookieType cookieType){
		AtmosphereCookie myCookie = getCookieByName(cookieName);
		myCookie.setMaxAge(0);
		myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());
		VaadinService.getCurrentResponse().addCookie(myCookie); 
	}

	private static AtmosphereCookie getCookieByName(String name) {
		// Fetch all cookies from the request
		Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();

		// Iterate to find cookie by its name
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName())) {
				return new AtmosphereCookie(cookie);
			}
		}

		return null;
	}
}
