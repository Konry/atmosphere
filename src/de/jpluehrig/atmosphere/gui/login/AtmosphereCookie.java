package de.jpluehrig.atmosphere.gui.login;

import java.util.Random;

import javax.servlet.http.Cookie;

public class AtmosphereCookie extends Cookie {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6878711265231125281L;

	private static final int NUMBER_OF_VALUES_IN_COOKIE = 3;

	Long id = null;

	String cookieName = null;

	String username = null;

	CookieType cookieType = null;

	public AtmosphereCookie(Cookie cookie) {
		super(cookie.getName(), "");
		parseValueIntoAtmosphereCookie(cookie);
	}

	public AtmosphereCookie(String cookieName, String username, CookieType cookieType) {
		super(cookieName, "");
		this.id = new Random().nextLong();
		this.cookieName = cookieName;
		this.username = username;
		this.cookieType = cookieType;
		setCookieValue();
	}

	private void setCookieValue() {
		this.setValue(username + "-#-" + cookieType + "-#-" + id.toString());
	}

	/**
	 * Warning cookieType can be null if the string is not recognized.
	 * @param cookie
	 */
	private void parseValueIntoAtmosphereCookie(Cookie cookie) {
		String[] split = cookie.getValue().split("-#-");
		if (split.length < NUMBER_OF_VALUES_IN_COOKIE || split.length > NUMBER_OF_VALUES_IN_COOKIE) {
			throw new IllegalArgumentException(
					"The cookie has not the same type as the expected AtmosphereCookie. Ignore this if you are not an administrator.");
		} else {
			this.username = split[0];
			this.cookieType = CookieType.parseCookieType(split[1]);
			this.id = Long.parseLong(split[2]);
		}
	}

	public void renewID() {
		this.id = new Random().nextLong();
	}

}
