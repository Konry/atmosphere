package de.jpluehrig.atmosphere.gui.login;

import javax.servlet.http.Cookie;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import de.jpluehrig.atmosphere.AtmosphereMain;
import de.jpluehrig.atmosphere.model.security.PasswordCrypt;

public class LoginView extends LoginScreen implements View {

	private static final String ATMOSPHERE_COOKIE_NAME = "atmosphere-last-login";

	private static final int secondsPerMonth = 60 * 60 * 24 * 31;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoginView() {
		checkRememberMeFunction();
		initButtonListener();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		AtmosphereMain.logger.debug("Enter Login View!");
	}

	public void initButtonListener() {
		AtmosphereMain.logger.debug("Update");
//		usernameTextField.setTabIndex(1);
//		passwordField.setTabIndex(2);
//		loginButton.setTabIndex(3);

		loginButton.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				logUserIn(usernameTextField.getValue());
			}
		});
	}

	private void logUserIn(String username) {
		checkRememberMeFunction();
		AtmosphereMain.logger.debug("log in with username: " + username + " " + passwordField.getValue());
		/* Important use here a https connection */

		/* Check password */

		/* Set cookie */
		CookieManager.createCookie(ATMOSPHERE_COOKIE_NAME, username, CookieType.LOGGEDIN);
		getSession().setAttribute("user", username);
		System.out.println("USER "+getSession().getAttribute("user"));
		getUI().getNavigator().navigateTo(AtmosphereMain.MAINVIEW);
	}

	private void checkRememberMeFunction() {
		if (!rememberMeCheckBox.getValue()) {
			expireCookie();
			return;
		}
		AtmosphereMain.logger.debug("Check Remember Function");
	}

	private void expireCookie() {
		printCookies();
	}

	private void logUserOut() {
		printCookies();
	}

	private void printCookies() {
		Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
		for (Cookie c : cookies) {
			System.out.println(c.getName());
			if (c.getName().equals(ATMOSPHERE_COOKIE_NAME)) {
				c.setMaxAge(0);
			}
		}
	}
}
