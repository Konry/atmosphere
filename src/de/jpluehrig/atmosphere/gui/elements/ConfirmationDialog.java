package de.jpluehrig.atmosphere.gui.elements;

import de.steinwedel.messagebox.MessageBox;

public class ConfirmationDialog {

	/**
	 * Creating an easy confirmation dialog which uses Callback functions for the confirmation or canceling process
	 * @param title The title is used as caption.
	 * @param text The text inside the confirmation view
	 * @param objectForCallback The object itself which implements the ConfirmView
	 * @param confirmActionName The identifier for the confirmation dialog useful if there are more than one confirmation dialog per view
	 */
	public ConfirmationDialog(String title, String text, ConfirmDialogCallback objectForCallback, String confirmActionName) {
		MessageBox.createQuestion()
		.withCaption(title)
		.withMessage(text)
		.withOkButton(() -> objectForCallback.ConfirmAction(confirmActionName))
		.withCancelButton(() -> objectForCallback.CancelAction(confirmActionName))
		.open();
	}
}
