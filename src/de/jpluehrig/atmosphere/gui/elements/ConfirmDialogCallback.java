package de.jpluehrig.atmosphere.gui.elements;

public interface ConfirmDialogCallback {

	public void CancelAction(String confirmActionName);
	
	public void ConfirmAction(String confirmActionName);
	
}
