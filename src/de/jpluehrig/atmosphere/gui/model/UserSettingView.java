package de.jpluehrig.atmosphere.gui.model;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import de.jpluehrig.atmosphere.AtmosphereMain;
import de.jpluehrig.atmosphere.gui.elements.ConfirmDialogCallback;
import de.jpluehrig.atmosphere.gui.elements.ConfirmationDialog;

public class UserSettingView extends UserSettingScreen implements ConfirmDialogCallback, View {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8989856352772883654L;
	
	public UserSettingView() {
		intitializeButtons();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}
	
	private void intitializeButtons(){
		cancelButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -7811187108616167455L;

			@Override
			public void buttonClick(ClickEvent event) {
				new ConfirmationDialog("Abbrechen", "Wollen Sie abbrechen und zur�ck zum Men�?", UserSettingView.this, "cancelButton");	
			}
		});
		
		saveButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 8155159946099843834L;

			@Override
			public void buttonClick(ClickEvent event) {
				new ConfirmationDialog("Speichern", "Wollen Sie speichern und zur�ck zum Men�?", UserSettingView.this, "confirmButton");	
			}
		});
	}

	@Override
	public void CancelAction(String confirmActionName) {
		AtmosphereMain.logger.debug("Cancel action of "+confirmActionName);
		switch(confirmActionName){
		case "cancelButton":
			break;
		case "saveButton":
			break;
		}
	}

	@Override
	public void ConfirmAction(String confirmActionName) {
		AtmosphereMain.logger.debug("Confirm action of "+confirmActionName);
		switch(confirmActionName){
		case "cancelButton":
			getUI().getNavigator().navigateTo(AtmosphereMain.MAINVIEW);
			break;
		case "saveButton":
			getUI().getNavigator().navigateTo(AtmosphereMain.MAINVIEW);
			break;
		}
	}

}
