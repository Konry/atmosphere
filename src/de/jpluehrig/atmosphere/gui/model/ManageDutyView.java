package de.jpluehrig.atmosphere.gui.model;

import com.google.gwt.user.client.ui.ClickListenerCollection;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.Container.ItemSetChangeListener;
import com.vaadin.data.Container.PropertySetChangeEvent;
import com.vaadin.data.Container.PropertySetChangeListener;
import com.vaadin.event.ContextClickEvent;
import com.vaadin.event.ContextClickEvent.ContextClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import de.jpluehrig.atmosphere.AtmosphereMain;

public class ManageDutyView extends ManageDutyScreen implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4035537688770199126L;

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}
	
	public ManageDutyView() {
		initialize();
	}

	private void initialize() {
		initActionListener();
		initExampleData();
	}

	private void initExampleData() {
		selectDutyList.addItems("Mercury", "Venus", "Earth");
		
	}

	private void initActionListener() {		
		backButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -7520524605243311083L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.warn("backButton - Click on button ");
				getUI().getNavigator().navigateTo(AtmosphereMain.MAINVIEW);
			}
		});
		
		changeSelectedDateButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -2069170931912054070L;

			@Override
			public void buttonClick(ClickEvent event) {
				AtmosphereMain.logger.warn("changeSelectedDateButton - Click on item "+selectDutyList.getValue());
				getUI().getNavigator().navigateTo(AtmosphereMain.ATTENDANTVIEW);
			}
		});
	}

}
