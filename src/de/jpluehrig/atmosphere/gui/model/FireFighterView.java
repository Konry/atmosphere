package de.jpluehrig.atmosphere.gui.model;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import de.jpluehrig.atmosphere.AtmosphereMain;
import de.jpluehrig.atmosphere.gui.elements.ConfirmDialogCallback;
import de.jpluehrig.atmosphere.gui.elements.ConfirmationDialog;

public class FireFighterView extends FireFighterScreen implements ConfirmDialogCallback, View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6117691143060569925L;
	
	public FireFighterView() {
		intitializeButtons();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

	private void intitializeButtons(){
		cancelButton.addClickListener(new ClickListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 6520607724510941597L;

			@Override
			public void buttonClick(ClickEvent event) {
				new ConfirmationDialog("Abbrechen", "Fortfahren", FireFighterView.this, "cancelButton");	
			}
		});
	}

	@Override
	public void CancelAction(String confirmActionName) {
		AtmosphereMain.logger.debug("Cancel action of "+confirmActionName);
		
	}

	@Override
	public void ConfirmAction(String confirmActionName) {
		AtmosphereMain.logger.debug("Confirm action of "+confirmActionName);
		
	}
}
