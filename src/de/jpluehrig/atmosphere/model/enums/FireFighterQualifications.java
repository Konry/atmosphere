package de.jpluehrig.atmosphere.model.enums;

public enum FireFighterQualifications {
	Funkausbildung,
	Atemschutzgeräteträger,
	Maschinist,
	Kettensägenführer,
	Feuerwehrführerschein,
}
