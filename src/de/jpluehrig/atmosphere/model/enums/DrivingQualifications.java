package de.jpluehrig.atmosphere.model.enums;

public enum DrivingQualifications {
	KLASSEB,
	KLASSEBE,
	KLASSEC1,
	KLASSEC1E,
	KLASSEC,
	KLASSECE,
}
