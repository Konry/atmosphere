package de.jpluehrig.atmosphere.model.enums;

public enum LeadingQualifications {
	Verbandsf�hrer,
	Zugf�hrer,
	Gruppenf�hrer,
	Truppf�hrer,
	Truppmann,
	Grundausbildung,
}
