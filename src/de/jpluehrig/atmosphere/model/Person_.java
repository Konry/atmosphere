package de.jpluehrig.atmosphere.model;

import de.jpluehrig.atmosphere.model.security.PasswordCrypt;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-01-23T14:07:37.929+0100")
@StaticMetamodel(Person.class)
public class Person_ {
	public static volatile SingularAttribute<Person, Long> id;
	public static volatile SingularAttribute<Person, String> name;
	public static volatile SingularAttribute<Person, String> forename;
	public static volatile SingularAttribute<Person, String> email;
	public static volatile SingularAttribute<Person, String> fotoid;
	public static volatile SingularAttribute<Person, String> location;
	public static volatile SingularAttribute<Person, String> loginName;
	public static volatile SingularAttribute<Person, PasswordCrypt> loginData;
}
