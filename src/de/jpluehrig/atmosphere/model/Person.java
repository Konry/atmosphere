package de.jpluehrig.atmosphere.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.jpluehrig.atmosphere.model.security.PasswordCrypt;


//@Entity
//@Table(name="PERSON")
public class Person {
	
	private long id;
	
	private String name;

	private String forename;

	private String email;
	
	private String fotoid;
	
	private String location;
	
	private String loginName;

	private PasswordCrypt loginData;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(length = 32)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 32)
	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	@Column(length = 32, nullable = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 64, nullable = true)
	public String getFotoid() {
		return fotoid;
	}

	public void setFotoid(String fotoid) {
		this.fotoid = fotoid;
	}
	
	@Column(length = 64, nullable = true)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(length = 32, unique = true, nullable = true)
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@OneToOne(targetEntity=de.jpluehrig.atmosphere.model.security.PasswordCrypt.class, optional=true)
	public PasswordCrypt getLoginData() {
		return loginData;
	}

	public void setLoginData(PasswordCrypt loginData) {
		this.loginData = loginData;
	}
}
