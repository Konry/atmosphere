package de.jpluehrig.atmosphere.model;

import de.jpluehrig.atmosphere.model.enums.DrivingQualifications;
import de.jpluehrig.atmosphere.model.enums.LeadingQualifications;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-01-23T13:58:28.228+0100")
@StaticMetamodel(FireFighter.class)
public class FireFighter_ extends Person_ {
	public static volatile SingularAttribute<FireFighter, Date> memberVoluntaryFireDepartmentGeneral;
	public static volatile SingularAttribute<FireFighter, Date> memberVoluntaryFireDepartment26;
	public static volatile SingularAttribute<FireFighter, Boolean> acceptedMember;
	public static volatile SingularAttribute<FireFighter, Boolean> activeSmokeDiver;
	public static volatile SingularAttribute<FireFighter, Boolean> active;
	public static volatile SingularAttribute<FireFighter, DrivingQualifications> drivingQualification;
	public static volatile SingularAttribute<FireFighter, LeadingQualifications> leadingQualification;
}
