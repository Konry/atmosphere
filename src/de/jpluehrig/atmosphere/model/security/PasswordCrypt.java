package de.jpluehrig.atmosphere.model.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Konry
 */
@Entity
@Table(name = "PasswordCrypt")
public class PasswordCrypt {
	public static final String PERSISTENCE_UNIT = "firedb"; 

	private long passwordId;

	private String hashedPassword = null;

	private String saltKey = null;

	public PasswordCrypt() {
		saltKey = getSalt();
	}

	public PasswordCrypt(String cleanPassword) {
		this();
		hashedPassword = cryptPassword(cleanPassword, saltKey);
	}

	public static String cryptPassword(String password, String salt) {
		String cryptedPassword = null;
		try {
			cryptedPassword = saltAndHashPassword(password, salt);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return cryptedPassword;
	}

	private static String saltAndHashPassword(String password, String salt) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		md.update(salt.getBytes());
		byte[] bytes = md.digest(password.getBytes());
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	private static String getSalt() {
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");

			byte[] salt = new byte[16];
			sr.nextBytes(salt);
			return salt.toString();
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean checkPassword(String password, PasswordCrypt storedPasswordCrypt) {
		try {
			System.out.println("Entered password: " + password);
			System.out.println(saltAndHashPassword(password, storedPasswordCrypt.saltKey));
			System.out.println(storedPasswordCrypt.hashedPassword);
			if (storedPasswordCrypt.hashedPassword.equals(saltAndHashPassword(password, storedPasswordCrypt.saltKey))) {
				return true;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getPasswordId() {
		return passwordId;
	}

	public void setPasswordId(long passwordId) {
		this.passwordId = passwordId;
	}

	@Column
	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	@Column
	public String getSaltKey() {
		return saltKey;
	}

	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	@Override
	public String toString() {
		return "PasswordCrypt [PasswordId=" + passwordId + ", hashedPassword=" + hashedPassword + ", saltKey=" + saltKey+ "]";
	}

}
