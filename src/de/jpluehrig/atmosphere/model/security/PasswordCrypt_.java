package de.jpluehrig.atmosphere.model.security;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-01-23T14:08:20.214+0100")
@StaticMetamodel(PasswordCrypt.class)
public class PasswordCrypt_ {
	public static volatile SingularAttribute<PasswordCrypt, String> hashedPassword;
	public static volatile SingularAttribute<PasswordCrypt, String> saltKey;
	public static volatile SingularAttribute<PasswordCrypt, Long> passwordId;
}
