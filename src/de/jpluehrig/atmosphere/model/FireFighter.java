package de.jpluehrig.atmosphere.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import de.jpluehrig.atmosphere.model.enums.DrivingQualifications;
import de.jpluehrig.atmosphere.model.enums.FireFighterQualifications;
import de.jpluehrig.atmosphere.model.enums.LeadingQualifications;


@Entity
@Table(name="FIREFIGHTER", catalog = "user")
public class FireFighter extends Person {
	public static final String PERSISTENCE_UNIT = "firedb"; 
	
	private Date memberVoluntaryFireDepartmentGeneral = null;
	
	private Date memberVoluntaryFireDepartment26 = null;
	
	private boolean acceptedMember = false;
	
	private boolean activeSmokeDiver = false;
	
	private boolean active = false;

	private DrivingQualifications drivingQualification = null;
	
	private LeadingQualifications leadingQualification = LeadingQualifications.Grundausbildung;
	
	private List<FireFighterQualifications> qualifications = new ArrayList<>();
	
	
	@Column(nullable = true)
	public Date getMemberVoluntaryFireDepartmentGeneral() {
		return memberVoluntaryFireDepartmentGeneral;
	}

	public void setMemberVoluntaryFireDepartmentGeneral(Date memberVoluntaryFireDepartmentGeneral) {
		this.memberVoluntaryFireDepartmentGeneral = memberVoluntaryFireDepartmentGeneral;
	}

	@Column(nullable = true)
	public Date getMemberVoluntaryFireDepartment26() {
		return memberVoluntaryFireDepartment26;
	}

	public void setMemberVoluntaryFireDepartment26(Date memberVoluntaryFireDepartment26) {
		this.memberVoluntaryFireDepartment26 = memberVoluntaryFireDepartment26;
	}

	@Column(nullable = true)
	public boolean isAcceptedMember() {
		return acceptedMember;
	}

	public void setAcceptedMember(boolean acceptedMember) {
		this.acceptedMember = acceptedMember;
	}

	@Column(nullable = true)
	public boolean isActiveSmokeDiver() {
		return activeSmokeDiver;
	}

	public void setActiveSmokeDiver(boolean activeSmokeDiver) {
		this.activeSmokeDiver = activeSmokeDiver;
	}

	@Column
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	public DrivingQualifications getDrivingQualification() {
		return drivingQualification;
	}

	public void setDrivingQualification(DrivingQualifications drivingQualification) {
		this.drivingQualification = drivingQualification;
	}

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	public LeadingQualifications getLeadingQualification() {
		return leadingQualification;
	}

	public void setLeadingQualification(LeadingQualifications leadingQualification) {
		this.leadingQualification = leadingQualification;
	}

	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	public List<FireFighterQualifications> getQualifications() {
		return qualifications;
	}

	public void setQualifications(List<FireFighterQualifications> qualifications) {
		this.qualifications = qualifications;
	}
}
